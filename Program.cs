﻿using System;
using System.Net.NetworkInformation;

namespace netinfo {

    static class Program {
        static void Main (string [] args) {
            foreach (var item in NetworkInterface.GetAllNetworkInterfaces ()) {
                Console.WriteLine ($"{item.Id} = {item.Name}");
            }
        }
    }
}
